#pragma once
#include <string>	
#include <vector>
#include "Types.h"
#include "Party.h"

enum class savingThrows {
	STRENGTH,
	DEXTERITY,
	CONSTITUTION,
	INTELLIGENCE,
	WISDOM,
	CHARISMA,
	NA,
};
enum class spellLevel {
	FIRST,
	SECOND,
	THIRD,
	FORTH,
	FIFTH,
	SIXTH,
	SEVENTH,
	EIGHTH,
	NINETH,
};
enum class castTimes {
	BONUSACTION,
	REACTION,
	ACTION,
};

struct spell {
	std::string name;
	spellLevel level;
	std::vector<double> effectAmount;
	damageTypes effect;
	savingThrows savingThrow;
	castTimes time;
};

// Healing
spell cureWounds = { "Cure Wounds", spellLevel::FIRST, {1,8}, damageTypes::HEAL, savingThrows::NA, castTimes::ACTION };

// Attack
spell burningHands = { "Burning Hands", spellLevel::FIRST, {3, 6}, damageTypes::FIRE, savingThrows::DEXTERITY, castTimes::ACTION };
spell catapult = { "Catapult", spellLevel::FIRST, {3, 8}, damageTypes::BLUDGEONING, savingThrows::DEXTERITY, castTimes::ACTION };

spell fireball = { "Fireball", spellLevel::THIRD, {8, 6}, damageTypes::FIRE, savingThrows::NA, castTimes::ACTION };

// Effect
spell animalFriendship = { "Animal Friendship", spellLevel::FIRST, {}, damageTypes::EFFECT, savingThrows::WISDOM, castTimes::ACTION };
spell alarm = { "Alarm", spellLevel::FIRST, {}, damageTypes::EFFECT, savingThrows::NA, castTimes::BONUSACTION };
spell absorbElements = { "Absorb Elements", spellLevel::FIRST, {1, 6}, damageTypes::EFFECT, savingThrows::NA, castTimes::REACTION };
spell armorOfAgathys = { "Armor of Agathys", spellLevel::FIRST, {5, 5}, damageTypes::EFFECT, savingThrows::NA, castTimes::ACTION };
spell armsOfHadar = { "Arms of Hadar", spellLevel::FIRST, {2, 6}, damageTypes::EFFECT, savingThrows::STRENGTH, castTimes::ACTION };
spell bane = { "Bane", spellLevel::FIRST, {1, 4}, damageTypes::EFFECT, savingThrows::CHARISMA, castTimes::ACTION };
spell bless = { "Bless", spellLevel::FIRST, {1, 4}, damageTypes::EFFECT, savingThrows::NA, castTimes::ACTION };
spell causeFear = { "Cause Fear", spellLevel::FIRST, {}, damageTypes::EFFECT, savingThrows::WISDOM, castTimes::ACTION };

 
std::vector<std::vector<spell>> bardSpells{
	// First Level Spells:
	{
		cureWounds, bane,
},
	// Second Level Spells:
	{

},
	// Third Level Spells:
	{

},
	// Forth Level Spells:
	{

},
	// Fifth Level Spells:
	{

},
	// Sixth Level Spells:
	{

},
	// Seventh Level Spells:
	{

},
	// Eighth Level Spells:
	{

},
	// Ninth Level Spells:
	{

},
};
std::vector<std::vector<spell>> clericSpells{
	// First Level Spells:
	{
		cureWounds, bane,
},
	// Second Level Spells:
	{

},
	// Second Level Spells:
	{

},
	// Forth Level Spells:
	{

},
	// Fifth Level Spells:
	{

},
	// Sixth Level Spells:
	{

},
	// Seventh Level Spells:
	{

},
	// Eighth Level Spells:
	{

},
	// Ninth Level Spells:
	{

},
};
std::vector<std::vector<spell>> druidSpells{
	// First Level Spells:
	{
		cureWounds, absorbElements,
},
	// Second Level Spells:
	{

},
	// Third Level Spells:
	{

},
	// Forth Level Spells:
	{

},
	// Fifth Level Spells:
	{

},
	// Sixth Level Spells:
	{

},
	// Seventh Level Spells:
	{

},
	// Eighth Level Spells:
	{

},
	// Ninth Level Spells:
	{

},
};
std::vector<std::vector<spell>> paladinSpells{
	// First Level Spells:
	{
		cureWounds,
},
	// Second Level Spells:
	{

},
	// Third Level Spells:
	{

},
	// Forth Level Spells:
	{

},
	// Fifth Level Spells:
	{

},
	// Sixth Level Spells:
	{

},
	// Seventh Level Spells:
	{

},
	// Eighth Level Spells:
	{

},
	// Ninth Level Spells:
	{

},
};
std::vector<std::vector<spell>> rangerSpells{
	// First Level Spells:
	{
		cureWounds, absorbElements,
},
	// Second Level Spells:
	{

},
	// Third Level Spells:
	{

},
	// Forth Level Spells:
	{

},
	// Fifth Level Spells:
	{

},
	// Sixth Level Spells:
	{

},
	// Seventh Level Spells:
	{

},
	// Eighth Level Spells:
	{

},
	// Ninth Level Spells:
	{

},
};
std::vector<std::vector<spell>> sorcerorSpells{
	// First Level Spells:
	{
		absorbElements,
},
	// Second Level Spells:
	{

},
	// Third Level Spells:
	{
		fireball,
},
	// Forth Level Spells:
	{

},
	// Fifth Level Spells:
	{

},
	// Sixth Level Spells:
	{

},
	// Seventh Level Spells:
	{

},
	// Eighth Level Spells:
	{

},
	// Ninth Level Spells:
	{

},
};
std::vector<std::vector<spell>> warlockSpells{
	// First Level Spells:
	{
		armorOfAgathys, armsOfHadar,
},
	// Second Level Spells:
	{

},
	// Third Level Spells:
	{

},
	// Forth Level Spells:
	{

},
	// Fifth Level Spells:
	{

},
	// Sixth Level Spells:
	{

},
	// Seventh Level Spells:
	{

},
	// Eighth Level Spells:
	{

},
	// Ninth Level Spells:
	{

},
};
std::vector<std::vector<spell>> wizardSpells{
	// First Level Spells:
	{
		absorbElements,
},
	// Second Level Spells:
	{

},
	// Third Level Spells:
	{
		fireball,
},
	// Forth Level Spells:
	{

},
	// Fifth Level Spells:
	{

},
	// Sixth Level Spells:
	{

},
	// Seventh Level Spells:
	{

},
	// Eighth Level Spells:
	{

},
	// Ninth Level Spells:
	{

},
};