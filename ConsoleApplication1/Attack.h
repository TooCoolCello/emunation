#pragma once
#include <string>
#include <vector>
#include "Types.h"
#include "Weapons.h"
#include "Armor.h"

struct attack {
	std::string name;
	elementalAttackTypes attackType;
	std::vector <std::vector<weapon>, std::vector<shield>> usableEquipment;
	double critChance;
	statusEffects effect;
	double effectChance;
};

attack slash = { "Slash", elementalAttackTypes::NORMAL, {{shortsword, handaxe, sickle, battleaxe, greataxe, greatsword, longsword, scimitar}, {}}, 17, statusEffects::BLEEDING, 19 };
attack arrowShot = { "Arrow Shot", elementalAttackTypes::NORMAL, {{shortbow, longbow}, {} }, 17, statusEffects::BLEEDING, 19 };
attack shieldBash = { "Shield Bash", elementalAttackTypes::NORMAL, {{}, {starterShield, woodenShield}}, 17, statusEffects::DIZZY, 19 };