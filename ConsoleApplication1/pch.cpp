#include "pch.h"
#include "Spells.h"
#include "Party.h"
#include "Warlock.h"
#include <vector>
#include <string>
#include <cmath>

std::vector<int> calcWarlockSlots (partyMember member) {
	int spellSlots = 0;
	int slotLevel = (member.level <= 9) ? (1 + int(std::trunc((member.level - 1) / 2))) : (5);
	if (member.level == 1) {
		spellSlots = 1;
	}
	else if (member.level <= 10) {
		spellSlots = 2;
	}
	else if (member.level <= 16) {
		spellSlots = 3;
	}
	else {
		spellSlots = 4;
	}
	return std::vector<int> { spellSlots, slotLevel };
};
std::vector<int> calcFullCasterSlots (partyMember member) {
	std::vector<int> spellSlotLevels {0, 0, 0, 0, 0, 0, 0, 0, 0};
	spellSlotLevels[0] = (member.level <= 2) ? ((member.level == 1) ? (2) : (3)) : (4);
	spellSlotLevels[1] = (member.level >= 3) ? ((member.level == 3) ? (2) : (3)) : (0);
	spellSlotLevels[2] = (member.level >= 5) ? ((member.level == 5) ? (2) : (3)) : (0);
	spellSlotLevels[3] = (member.level >= 7) ? ((member.level == 7) ? (1) : ((member.level == 8) ? (2) : (3))) : (0);
	spellSlotLevels[4] = (member.level >= 9) ? ((member.level == 9) ? (1) : ((member.level < 18) ? (2) : (3))) : (0);
	spellSlotLevels[5] = (member.level >= 11) ? ((member.level < 19) ? (1) : (2)) : (0);
	spellSlotLevels[6] = (member.level >= 13) ? ((member.level < 20) ? (1) : (2)) : (0);
	spellSlotLevels[7] = (member.level >= 15) ? (1) : (0);
	spellSlotLevels[8] = (member.level >= 17) ? (1) : (0);
	return spellSlotLevels;
};
std::vector<int> calcHalfCasterSlots(partyMember member) {
	std::vector<int> spellSlotLevels{ 0, 0, 0, 0, 0 };
	spellSlotLevels[0] = (member.level >= 2) ? ((member.level == 2) ? (2) : (member.level < 5) ? (3) : (4)) : (0);
	spellSlotLevels[1] = (member.level >= 5) ? ((member.level < 7) ? (2) : (3)) : (0);
	spellSlotLevels[2] = (member.level >= 9) ? ((member.level < 11) ? (2) : (3)) : (0);
	spellSlotLevels[3] = (member.level >= 13) ? ((member.level < 15) ? (1) : (member.level < 17) ? (2) : (3)) : (0);
	spellSlotLevels[4] = (member.level >= 17) ? ((member.level < 19) ? (1) : (2)) : (0);
	return spellSlotLevels;
}