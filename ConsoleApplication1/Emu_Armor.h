#pragma once
#include <string>
#include <vector>

struct emuArmor {
	armorMaterials material;
	double armorPoints;
	double durability;
	bool initiativeDisadvantage;
};

emuArmor paddedArmor = { armorMaterials::PADDED, 11, 275, true};
emuArmor leatherArmor = { armorMaterials::LEATHER, 11, 300, false};
emuArmor hideArmor = { armorMaterials::HIDE, 12, 325, false };
emuArmor scaleArmor = { armorMaterials::SCALE, 14, 375, true };
emuArmor chainArmor = { armorMaterials::CHAINMAIL, 15, 525, true };
emuArmor plateArmor = { armorMaterials::PLATE, 18, 550, true };