#pragma once
#include <string>
#include <vector>
#include "Emu_Attacks.h"
#include "Emu_Armor.h"
#include "Weapons.h"

enum class emuTypes {
	FIRE,
	WATER,
	GROUND,
	AIR,
	ELECTRIC,
	FIGHTING,
	POISON,
	GHOST,
	METAL,
	GRASS,
	PHYCHIC,
	ICE,
	DARK,
	LIGHT,
};
struct emu {
	std::string name;
	std::vector <emuTypes> type;
	std::vector<int> level;
	double health;
	double strength;
	double speed;
	emuArmor armor;
	std::vector<elementalAttackTypes> weaknesses;
	std::vector<elementalAttackTypes> resistances;
	std::vector<elementalAttackTypes> immunities;
};

// Elemental Emus | Name | Type | Level | Health | Strength | Speed | Armor | Weaknesses | Ressistances | Immunities
emu fireEmu = { "Fire Emu", {emuTypes::FIRE}, {9,9}, 85, 1, 30, chainArmor, {elementalAttackTypes::WATER}, {elementalAttackTypes::FIRE}, {} };
emu waterEmu = { "Water Emu", {emuTypes::WATER}, {1,2}, 125, 1, 30, leatherArmor, {elementalAttackTypes::ELECTRIC}, {elementalAttackTypes::WATER}, {} };
emu groundEmu = { "Ground Emu", {emuTypes::GROUND}, {1,2}, 150, 2, 25, paddedArmor, {elementalAttackTypes::WATER}, {elementalAttackTypes::GROUND}, {} };
emu airEmu = { "Air Emu", {emuTypes::AIR}, {1,2}, 100, 1, 40, leatherArmor, {elementalAttackTypes::FIRE}, {elementalAttackTypes::AIR}, {} };
emu electricEmu = { "Electric Emu", {emuTypes::ELECTRIC}, {1,2}, 100, 1, 45, paddedArmor, {elementalAttackTypes::GROUND}, {elementalAttackTypes::ELECTRIC}, {} };
emu fightingEmu = { "Fighting Emu", {emuTypes::FIGHTING}, {1,2}, 80, 3, 35, hideArmor, {elementalAttackTypes::ELECTRIC}, {elementalAttackTypes::DARK}, {} };
emu egyptianEmu = { "Egyptian Emu", {emuTypes::FIRE, emuTypes::GROUND}, {20,20}, 135, 1, 25, leatherArmor, {elementalAttackTypes::WATER}, {elementalAttackTypes::GROUND, elementalAttackTypes::FIRE}, {} };

std::vector<emu> Emus{
	fireEmu, waterEmu, groundEmu, airEmu, electricEmu, fightingEmu,
};