#include "pch.h"
#include <iostream>
#include <vector>
#include <array>
#include <string>
#include <random>
#include <ctime>
#include <chrono>
#include <thread>
#include <algorithm>
#include "Armor.h"
#include "Weapons.h"
#include "Party.h"
#include "Emus.h"
#include "Emu_Armor.h"
#include "Emu_Attacks.h"

int mersenneTwister(int lowRoll, int highRoll) {
	std::mt19937 generator(static_cast<unsigned int>(std::time(NULL)));
	std::uniform_real_distribution<double> dis(lowRoll, highRoll);
	int roll = int(dis(generator));
	return roll;
}
double calcAP(partyMember member) {
	double totalAP;
	totalAP = 0;
	for (int i = 0; i < 5; i = i + 1) {
		totalAP += member.equippedArmor[i].armorPoints;
	};
	if (member.isShieldEquipped) {
		if (member.playerClass == classes::SHIELDMASTER) {
			totalAP += member.equippedShield[0].armorPoints * 2;
		}
		else {
			totalAP += member.equippedShield[0].armorPoints;
		};
	}
	if (totalAP > 20) {
		totalAP = 20;
	}
	return totalAP;
}
double rollDie(int Die) {
	double highRoll = 0;
	switch (Die) {
	case 20: {
		highRoll = 21;
		break;
	}
	case 12: {
		highRoll = 13;
		break;
	}
	case 10: {
		highRoll = 11;
		break;
	}
	case 8: {
		highRoll = 9;
		break;
	}
	case 6: {
		highRoll = 7;
		break;
	}
	case 4: {
		highRoll = 5;
		break;
	}
	}
	return mersenneTwister (1, highRoll);
}
double calcSpell(partyMember member) {
	std::cout << "WIP!";
	int test;
	test = 1;
	return test;
}
double calcAtk(partyMember member) {
	double totalAtk;
	totalAtk = 0;
	bool crit;
	crit = false;
	int dieRolls;
	dieRolls = 1;
	int dieRolled = rollDie(20);
	for (int i = 0; i < member.equippedWeapons.size(); i = i + 1) {
		if (dieRolled == 1) {
			dieRolls = 0;
			std::cout << "You missed!";
		}
		else if (dieRolled >= member.equippedWeapons[0].critChance) {
			dieRolls = 2;
			std::cout << "It was a critical hit!" << std::endl;
			std::this_thread::sleep_for(std::chrono::milliseconds(1000));
		}
		else {
			dieRolls = 1;
		}
		for (int j = 0; j < member.equippedWeapons[i].damage[0] * dieRolls; j = j + 1) {
			totalAtk = totalAtk + rollDie(member.equippedWeapons[i].damage[1]);
		}
	}
	totalAtk = totalAtk * member.strength;
	return totalAtk;
}
std::vector<emuAttack> chooseEmuAtk(emu member) {
	int vectorIndex = 0;
	std::vector<emuAttack> usableAttacks;
	for (int k = 0; k < normalEmuAttacks.size(); k = k + 1) {
		usableAttacks.push_back(normalEmuAttacks[k]);
	}
	for (int i = 0; i < member.type.size(); i = i + 1) {
		switch (member.type[i]) {
		case emuTypes::FIRE: {
			vectorIndex = 1;
			break; }
		case emuTypes::WATER: {
			vectorIndex = 2;
			break; }
		case emuTypes::GROUND: {
			vectorIndex = 3;
			break; }
		case emuTypes::AIR: {
			vectorIndex = 4;
			break; }
		case emuTypes::ELECTRIC: {
			vectorIndex = 5;
			break; }
		case emuTypes::FIGHTING: {
			vectorIndex = 6;
			break; }
		};
		for (int k = 0; k < allEmuAttacks[vectorIndex].size(); k = k + 1) {
			usableAttacks.push_back(allEmuAttacks[vectorIndex][k]);
		};
	};
	std::vector<emuAttack> chosenAttacks;
	chosenAttacks.clear();
	int maxAttacks;
	int emuLevel = mersenneTwister(member.level[0], member.level[1]);
	if (emuLevel < 5)
		maxAttacks = 1;
	else if (emuLevel > 5 && emuLevel < 10)
		maxAttacks = 2;
	else if (emuLevel > 10 && emuLevel < 15)
		maxAttacks = 3;
	else if (emuLevel > 15)
		maxAttacks = 4;

	std::vector<int> usedAttackIndex;
	for (int i = 0; i < maxAttacks; i = i + 1) {
		bool newAttack = true;
		do {
			int attackIndex = mersenneTwister(0, usableAttacks.size());
			if (std::find(usedAttackIndex.begin(), usedAttackIndex.end(), attackIndex) != usedAttackIndex.end()) {
				newAttack = true;
			}
			else {
				newAttack = false;
				usedAttackIndex.push_back(attackIndex);
				chosenAttacks.push_back(usableAttacks[attackIndex]);
			}
		} while (newAttack);
		std::this_thread::sleep_for(std::chrono::milliseconds(1000));
	}
	
	return chosenAttacks;
}
double calcEmuAtk(emuAttack attack, emu member) {
	double totalEmuAtk;
	totalEmuAtk = 0;
	bool crit;
	crit = false;
	int dieRolls;
	dieRolls = 1;
	if (rollDie(20) >= attack.critChance) {
		dieRolls = 2;
		std::cout << "It was a critical hit!" << std::endl;
	}
	for (int j = 0; j < (attack.damage[0] * dieRolls); j = j + 1) {
		totalEmuAtk = totalEmuAtk + rollDie(attack.damage[1]);
	}
	totalEmuAtk = totalEmuAtk * member.strength;
	return totalEmuAtk;
}

int main() {
	std::cout << calcAtk(BrarackOrbrama);
}
