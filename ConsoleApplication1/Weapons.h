#pragma once
#include <vector>
#include <string>

enum class weaponProperties {
	FINESSE,
	HEAVY,
	LIGHT,
	RANGE,
	TWOHANDED,
	VERSATILE,
	THROWN,
};
enum class weaponDmgTypes {
	BLUDGEONING,
	PIERCING,
	SLASHING,
};

struct weapon {
	std::string name;
	std::vector<double> damage;
	weaponDmgTypes dmgType;
	damageTypes attackTypes;
	double critChance;
	statusEffects effect;
	double effectChance;
	std::vector<weaponProperties> properties;
};

// Simple Melee Weapons | Name | Damage | DmgType | Property 1 | Property 2 | Property 3 | Effect Chance
weapon club = { "Club", {1,4}, weaponDmgTypes::BLUDGEONING, damageTypes::NORMAL, 17, statusEffects::DIZZY, 18, {weaponProperties::LIGHT,} };
weapon dagger = { "Dagger", {1,4}, weaponDmgTypes::PIERCING, damageTypes::NORMAL, 17, statusEffects::BLEEDING, 18, {weaponProperties::LIGHT, weaponProperties::FINESSE, weaponProperties::THROWN} };
weapon handaxe = { "Hand-Axe", {1,6}, weaponDmgTypes::SLASHING, damageTypes::NORMAL, 17, statusEffects::NA, 18, {weaponProperties::LIGHT, weaponProperties::THROWN} };
weapon javelin = { "Javelin", {1,6}, weaponDmgTypes::PIERCING, damageTypes::NORMAL, 17, statusEffects::NA, 18, {weaponProperties::THROWN} };
weapon mace = { "Mace", {1,6}, weaponDmgTypes::BLUDGEONING, damageTypes::NORMAL, 17, statusEffects::NA, 18, {} };
weapon quarterstaff = { "Quarterstaff", {1,6}, weaponDmgTypes::BLUDGEONING, damageTypes::NORMAL, 17, statusEffects::NA, 18, {weaponProperties::VERSATILE} };
weapon sickle = { "Sickle", {1,4}, weaponDmgTypes::SLASHING, damageTypes::NORMAL, 17, statusEffects::NA, 18, {weaponProperties::LIGHT} };
weapon spear = { "Spear", {1,6}, weaponDmgTypes::PIERCING, damageTypes::NORMAL, 17, statusEffects::NA, 18, {weaponProperties::THROWN, weaponProperties::VERSATILE} };


//Simple Ranged Weapons | Name | Damage | DmgType | Property 1 | Property 2 | Property 3 | Effect Chance
weapon lightCrossbow = { "Light Crossbow", {1,8}, weaponDmgTypes::PIERCING, damageTypes::NORMAL, 17, statusEffects::NA, 18, {weaponProperties::TWOHANDED} };
weapon dart = { "Dart", {1,4}, weaponDmgTypes::PIERCING, damageTypes::NORMAL, 17, statusEffects::NA, 18, {weaponProperties::FINESSE, weaponProperties::THROWN} };
weapon shortbow = { "Short Bow", {1,6}, weaponDmgTypes::PIERCING, damageTypes::NORMAL, 17, statusEffects::NA, 18, {weaponProperties::TWOHANDED} };

//Martial Melee Weapons | Name | Damage | DmgType | Property 1 | Property 2 | Property 3 | Effect Chance
weapon battleaxe = { "Battleaxe", {1,8}, weaponDmgTypes::SLASHING, damageTypes::NORMAL, 17, statusEffects::NA, 18, {weaponProperties::VERSATILE} };
weapon flail = { "Flail", {1,8}, weaponDmgTypes::BLUDGEONING, damageTypes::NORMAL, 17, statusEffects::NA, 18, {} };
weapon greataxe = { "Greataxe", {1,12}, weaponDmgTypes::SLASHING, damageTypes::NORMAL, 17, statusEffects::NA, 18, {weaponProperties::HEAVY, weaponProperties::TWOHANDED} };
weapon greatsword = { "Greatsword", {2,6}, weaponDmgTypes::SLASHING, damageTypes::NORMAL, 17, statusEffects::NA, 18, {weaponProperties::HEAVY, weaponProperties::TWOHANDED} };
weapon lance = { "Lance", {1,12}, weaponDmgTypes::PIERCING, damageTypes::NORMAL, 17, statusEffects::NA, 18, {weaponProperties::TWOHANDED} };
weapon longsword = { "Longsword", {1,8}, weaponDmgTypes::SLASHING, damageTypes::NORMAL, 17, statusEffects::NA, 18, {weaponProperties::VERSATILE} };
weapon maul = { "Maul", {2,6}, weaponDmgTypes::BLUDGEONING, damageTypes::NORMAL, 17, statusEffects::NA, 18, {weaponProperties::HEAVY, weaponProperties::TWOHANDED} };
weapon pike = { "Pike", {1,10}, weaponDmgTypes::PIERCING, damageTypes::NORMAL, 17, statusEffects::NA, 18, {weaponProperties::HEAVY, weaponProperties::TWOHANDED} };
weapon rapier = { "Rapier", {1,8}, weaponDmgTypes::PIERCING, damageTypes::NORMAL, 17, statusEffects::NA, 18, {weaponProperties::FINESSE} };
weapon scimitar = { "Scimitar", {1,6}, weaponDmgTypes::SLASHING, damageTypes::NORMAL, 17, statusEffects::NA, 18, {weaponProperties::FINESSE, weaponProperties::LIGHT} };
weapon shortsword = { "Shortsword", {1,6}, weaponDmgTypes::SLASHING, damageTypes::NORMAL, 17, statusEffects::NA, 18, {weaponProperties::FINESSE, weaponProperties::LIGHT} };
weapon trident = { "Trident", {1,6}, weaponDmgTypes::PIERCING, damageTypes::NORMAL, 17, statusEffects::NA, 18, {weaponProperties::THROWN, weaponProperties::VERSATILE} };
weapon warhammer = { "Warhammer", {1,8}, weaponDmgTypes::BLUDGEONING, damageTypes::NORMAL, 17, statusEffects::NA, 18, {weaponProperties::TWOHANDED, weaponProperties::VERSATILE} };

// Martial Ranged Weapons | Name | Damage | DmgType | Property 1 | Property 2 | Property 3 | Effect Chance
weapon crossbow = { "Crossbow", {1,6}, weaponDmgTypes::PIERCING, damageTypes::NORMAL, 17, statusEffects::NA, 18, {weaponProperties::LIGHT} };
weapon heavyCrossbow = { "Heavy Crossbow", {1,10}, weaponDmgTypes::PIERCING, damageTypes::NORMAL, 17, statusEffects::NA, 18, {weaponProperties::HEAVY, weaponProperties::TWOHANDED} };
weapon longbow = { "Longbow", {1,8}, weaponDmgTypes::PIERCING, damageTypes::NORMAL, 17, statusEffects::NA, 18, {weaponProperties::HEAVY, weaponProperties::TWOHANDED} };

std::vector<weapon> simpleMeleeWeapons{
	club, dagger, handaxe, javelin, mace, quarterstaff, sickle, spear
};
std::vector<weapon> simpleRangedWeapons{
	lightCrossbow, dart, shortbow
};
std::vector<weapon> martialMeleeWeapons{
	battleaxe, flail, greataxe, greatsword, lance, longsword, maul, pike, rapier, scimitar, shortsword, trident, warhammer
};
std::vector<weapon> martialRangedWeapons{
	crossbow, heavyCrossbow, longbow
};