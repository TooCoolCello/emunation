#pragma once
#include <string>
#include <vector>
#include <array>
#include <map>
#include "Armor.h"
#include "Weapons.h"

enum class classes {
	BARBARIAN,
	BARD,
	CLERIC,
	DRUID,
	FIGHTER,
	MONK,
	PALADIN,
	RANGER,
	ROGUE,
	WIZARD,
	WARLOCK,
	SORCEROR,
	SHIELDMASTER,
};
enum class races {
	TEST,
};

struct partyMember {
	std::string name;
	classes playerClass;
	races playerRace;
	int level;
	double health;
	double speed;
	double strength;
	int spellSlots;
	std::map <std::string, int> stats;
	std::vector<weapon> equippedWeapons;
	std::vector<armor> equippedArmor;
	bool isShieldEquipped;
	std::vector<shield> equippedShield;
	double AP;
};

partyMember BrarackOrbrama{ "Brarack Orbrama", classes::BARBARIAN, races::TEST, 1, 200, 25, 2, 0, {{"Strength", 8}, {"Dexterity", 8}, {"Constitution", 8}, {"Intelligence", 8},{"Wisdom", 8}, {"Charisma", 8}}, {club,club}, {plateHelmet, plateChestplate, plateGauntlets, plateLeggings, plateBoots}, true, {woodenShield}, 0 };
partyMember JhananWhack{ "Jhanan Whack", classes::WARLOCK, races::TEST, 1, 100, 30, 1, 0, {{"Strength", 8}, {"Dexterity", 8}, {"Constitution", 8}, {"Intelligence", 8},{"Wisdom", 8}, {"Charisma", 8}}, {dagger}, {leatherHelmet, leatherChestplate, leatherGloves, leatherLeggings, leatherBoots}, false, {}, 0 };