#pragma once
#include <vector>
#include <string>
#include "Types.h"

struct emuAttack {
	std::string name;
	std::vector<double> damage;
	double manaUsage;
	elementalAttackTypes type;
	double critChance;
	statusEffects effect;
	double statusEffect;
};

// Normal
emuAttack Tackle = { "Tackle", {1,8}, 5, elementalAttackTypes::NORMAL, 17, statusEffects::NA, 0 };
emuAttack Kick = { "Kick", {1,8}, 5, elementalAttackTypes::NORMAL, 17, statusEffects::NA, 0 };
emuAttack Pound = { "Pound", {1,8}, 5, elementalAttackTypes::NORMAL, 17, statusEffects::NA, 0 };

// Fire
emuAttack Flare = { "Flare", {1,4}, 3, elementalAttackTypes::FIRE, 17, statusEffects::BURNT, 19 };
emuAttack Melt = { "Melt", {1,8}, 5, elementalAttackTypes::FIRE, 17, statusEffects::BURNT, 19 };
emuAttack Eruption = { "Eruption", {1,12}, 20, elementalAttackTypes::FIRE, 17, statusEffects::BURNT, 19 };

// Water
emuAttack Bubble = { "Bubble", {1,8}, 5, elementalAttackTypes::WATER, 17, statusEffects::NA, 0 };

// Ground
emuAttack rockThrow = { "Rock Throw", {1,8}, 5, elementalAttackTypes::GROUND, 17, statusEffects::DIZZY, 19 };

// Air
emuAttack Gust = { "Gust", {1,4}, 3, elementalAttackTypes::AIR, 17, statusEffects::NA, 0 };
emuAttack Whirlwind = { "Whirlwind", {1,8}, 5, elementalAttackTypes::AIR, 17, statusEffects::NA, 0 };

// Electric
emuAttack Shock = { "Shock", {1,8}, 5, elementalAttackTypes::ELECTRIC, 17, statusEffects::PARALYZED, 19 };
emuAttack Zap = { "Zap", {1, 4}, 3, elementalAttackTypes::ELECTRIC, 17, statusEffects::NA, 0 };
emuAttack Storm = { "Storm", {1, 10}, 15, elementalAttackTypes::ELECTRIC, 17, statusEffects::PARALYZED, 19 };

// Fighting
emuAttack Punch = { "Punch", {1,10}, 5, elementalAttackTypes::FIGHTING, 17, statusEffects::DIZZY, 19 };

std::vector <emuAttack> normalEmuAttacks{
	Tackle, Kick, Pound,
};
std::vector <emuAttack> fireEmuAttacks{
	Flare, Melt, Eruption,
};
std::vector <emuAttack> waterEmuAttacks{
	Bubble,
};
std::vector <emuAttack> groundEmuAttacks{
	rockThrow,
};
std::vector <emuAttack> airEmuAttacks{
	Gust, Whirlwind,
};
std::vector <emuAttack> electricEmuAttacks{
	Shock, Zap, Storm,
};
std::vector <emuAttack> fightingEmuAttacks{
	Punch,
};

std::vector<std::vector<emuAttack>> allEmuAttacks{
	normalEmuAttacks, fireEmuAttacks, waterEmuAttacks, groundEmuAttacks, airEmuAttacks, electricEmuAttacks, fightingEmuAttacks
};